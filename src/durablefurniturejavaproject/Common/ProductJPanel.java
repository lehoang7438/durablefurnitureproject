/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package durablefurniturejavaproject.Common;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import durablefurniturejavaproject.Bussiness.Product;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Redmibook 14
 */
public class ProductJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CategoryJPanel
     */
    public ProductJPanel() {
        initComponents();
        try {
            showTableProduct("");
        } catch (SQLException ex) {
            Logger.getLogger(ProductJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        txtProductId = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtProductPrice = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtProductName = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        cbBoxCategory = new javax.swing.JComboBox<>();
        jComboBox7 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        txtFieldAddColor1 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        btnAddMaterial = new javax.swing.JButton();
        btnAddColor = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        cbBoxMaterial = new javax.swing.JComboBox<>();
        cbBoxProdColor = new javax.swing.JComboBox<>();
        cbBoxAddSize = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        btnAddSize = new javax.swing.JButton();
        txtFieldAddColor = new javax.swing.JTextField();
        txtFieldAddMaterial = new javax.swing.JTextField();
        txtFieldAddSize = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();

        setBackground(new java.awt.Color(40, 81, 163));
        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setPreferredSize(new java.awt.Dimension(1040, 645));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 81, 163));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setPreferredSize(new java.awt.Dimension(1050, 640));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(1010, 150));

        tblProduct.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Product ID", "Product Name", "Category", "Brand", "Size", "Material", "Color", "Price", "Quantity"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProduct.setRequestFocusEnabled(false);
        jScrollPane1.setViewportView(tblProduct);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        txtProductId.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtProductId, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 230, -1));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Description");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, -1, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Product Price");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, -1, -1));

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Add/Edit");
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 550, 140, 40));

        txtProductPrice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtProductPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, 230, -1));

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Product Image");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 210, -1, -1));

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 480, 820, -1));

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton2.setText("Gallery");
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 400, 130, 20));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 240, 280, 180));

        txtProductName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtProductName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 320, 230, -1));

        jTextField5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(jTextField5, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 420, 210, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Product Name");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Product Quantity");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 400, -1, -1));

        jComboBox3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(jComboBox3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 360, 210, -1));

        cbBoxCategory.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbBoxCategory.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cbBoxCategory, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 240, 210, -1));

        jComboBox7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox7.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(jComboBox7, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 300, 210, -1));

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Product ID");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        jLabel9.setBackground(new java.awt.Color(255, 255, 255));
        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Product Category");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 220, -1, -1));

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Product Brand");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 280, -1, -1));

        jLabel11.setBackground(new java.awt.Color(255, 255, 255));
        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Product Status");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, -1, -1));

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton3.setText("Choose Image");
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 210, 130, 20));

        txtFieldAddColor1.setText("jTextField2");
        jPanel1.add(txtFieldAddColor1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 260, 210, 0));

        jPanel2.setBackground(new java.awt.Color(40, 81, 163));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Product Size");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        btnAddMaterial.setText("Add");
        btnAddMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddMaterialMouseClicked(evt);
            }
        });
        jPanel2.add(btnAddMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, 70, 20));

        btnAddColor.setText("Add");
        btnAddColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddColorActionPerformed(evt);
            }
        });
        jPanel2.add(btnAddColor, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, 70, 20));

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Product Material");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, -1));

        cbBoxMaterial.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbBoxMaterial.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbBoxMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBoxMaterialActionPerformed(evt);
            }
        });
        jPanel2.add(cbBoxMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 210, -1));

        cbBoxProdColor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbBoxProdColor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cbBoxProdColor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 210, -1));

        cbBoxAddSize.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbBoxAddSize.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cbBoxAddSize, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 210, -1));

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Product Color");
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        btnAddSize.setText("Add");
        btnAddSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSizeActionPerformed(evt);
            }
        });
        jPanel2.add(btnAddSize, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 200, 70, 20));

        txtFieldAddColor.setText("jTextField2");
        jPanel2.add(txtFieldAddColor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 210, 0));

        txtFieldAddMaterial.setText("jTextField2");
        jPanel2.add(txtFieldAddMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 210, 0));

        txtFieldAddSize.setText("jTextField2");
        jPanel2.add(txtFieldAddSize, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 210, 0));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 200, 240, 270));

        jButton4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton4.setText("Delete");
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 480, 140, 40));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
     List<Product> lstPrd = new ArrayList();

    DefaultTableModel tableModel;

   public void showTableProduct(String txtSearch) throws SQLException {
        tableModel = (DefaultTableModel) tblProduct.getModel();
        Product prod = new Product();

        tableModel.setRowCount(0);

        if (txtSearch.equals("")) {
            lstPrd = prod.getListProduct();
            System.out.println(lstPrd.size());
            for (Product prd : lstPrd) {
                tableModel.addRow(new Object[]{prd.getProductId(), prd.getProductName(), prd.getCategoryId(), prd.getBrandId(), prd.getMaterial(), prd.getColor(), prd.getSize(), prd.getPrice(), prd.getQuantity()});
            }
        } else {
            lstPrd = prod.getListProduct(txtSearch);
            for (Product prd : lstPrd) {
                tableModel.addRow(new Object[]{prd.getProductId(), prd.getProductName(), prd.getCategoryId(), prd.getBrandId(), prd.getMaterial(), prd.getColor(), prd.getSize(), prd.getPrice(), prd.getQuantity()});
            }
        }
    }
    static Boolean txtFieldAddColorIsShowing = false;
    static Boolean txtFieldAddMaterialIsShowing = false;
    private void btnAddMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMaterialMouseClicked
        if (txtFieldAddMaterialIsShowing == false) {
            txtFieldAddMaterialIsShowing = true;
            txtFieldAddMaterial.setBounds(10, 150, 210, 23);
        } else {

            txtFieldAddMaterialIsShowing = false;
            txtFieldAddMaterial.setBounds(10, 150, 210, 0);
            if (!txtFieldAddMaterial.getText().equals("")) {
                cbBoxMaterial.addItem(txtFieldAddMaterial.getText());
            }

        }
    }//GEN-LAST:event_btnAddMaterialMouseClicked
    static Boolean txtFieldAddSizeIsShowing = false;
    private void btnAddColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddColorActionPerformed
        if (txtFieldAddColorIsShowing == false) {
            txtFieldAddColorIsShowing = true;
            txtFieldAddColor.setBounds(10, 63, 210, 23);
        } else {

            txtFieldAddColorIsShowing = false;
            txtFieldAddColor.setBounds(10, 63, 210, 0);
            if (!txtFieldAddColor.getText().equals("")) {
                cbBoxProdColor.addItem(txtFieldAddColor.getText());
            }

        }

    }//GEN-LAST:event_btnAddColorActionPerformed

    private void cbBoxMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBoxMaterialActionPerformed
        if (txtFieldAddMaterialIsShowing == false) {
            txtFieldAddMaterialIsShowing = true;
            txtFieldAddMaterial.setBounds(10, 150, 210, 23);
        } else {

            txtFieldAddMaterialIsShowing = false;
            txtFieldAddMaterial.setBounds(10, 150, 210, 0);
            if (!txtFieldAddMaterial.getText().equals("")) {
                cbBoxMaterial.addItem(txtFieldAddMaterial.getText());
            }

        }
    }//GEN-LAST:event_cbBoxMaterialActionPerformed

    private void btnAddSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSizeActionPerformed
        if (txtFieldAddSizeIsShowing == false) {
            txtFieldAddSizeIsShowing = true;
            txtFieldAddSize.setBounds(10, 240, 210, 23);
        } else {

            txtFieldAddSizeIsShowing = false;
            txtFieldAddSize.setBounds(10, 240, 210, 0);
            if (!txtFieldAddSize.getText().equals("")) {
                cbBoxAddSize.addItem(txtFieldAddSize.getText());
            }

        }
    }//GEN-LAST:event_btnAddSizeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddColor;
    private javax.swing.JButton btnAddMaterial;
    private javax.swing.JButton btnAddSize;
    private javax.swing.JComboBox<String> cbBoxAddSize;
    private javax.swing.JComboBox<String> cbBoxCategory;
    private javax.swing.JComboBox<String> cbBoxMaterial;
    private javax.swing.JComboBox<String> cbBoxProdColor;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTable tblProduct;
    private javax.swing.JTextField txtFieldAddColor;
    private javax.swing.JTextField txtFieldAddColor1;
    private javax.swing.JTextField txtFieldAddMaterial;
    private javax.swing.JTextField txtFieldAddSize;
    private javax.swing.JTextField txtProductId;
    private javax.swing.JTextField txtProductName;
    private javax.swing.JTextField txtProductPrice;
    // End of variables declaration//GEN-END:variables
}
